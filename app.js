let bool = false;

let balancesElement = document.getElementById("totalBalance");
let loansElement = document.getElementById("getLoan");
const totalElement = document.getElementById("totalPay");
const loanElement = document.getElementById("totalLoan");
const bankElement = document.getElementById("bank");
const workElement = document.getElementById("work");
const computersElement = document.getElementById("computers");
const selectedComputerElement = document.getElementById("computerTitle");
let computersPriceElement = document.getElementById("computerPrice");
const computersPicElement = document.getElementById("pic");
const buyElement = document.getElementById("buyNow");
const repayLoanElement = document.getElementById("repayLoan");
const compFeaturesElement = document.getElementById("computerFeatures");
const compSpecsElement = document.getElementById("specs");

let balance = 0;
let salary = 0;
let loanAmount = 0;
let hasLoan = 0;
let loan = 0;
let computerPrices = 0;

const payPerClick = 100;

let computers = [];

let totalBalance = 0.0;

let currency = 0;

// Currency converter
let currencyValue = new Intl.NumberFormat('sv-SE', {style: 'currency', currency: 'SEK'});

// Read's data from API, with a try/catch function
fetch('https://noroff-komputer-store-api.herokuapp.com/computers') 
    .then(res => {
        if(!res.ok){
            throw new Error(`HTTP error! status: ${res.status}`);
        }
        else{
            res.json()
            .then(data => computers = data)
            .then(computers => addComputersToSite(computers));
        } 
    })
const addComputersToSite = (computers) => {
    computers.forEach(x => addComputerToSite(x));
    computersPriceElement=computers[0].price;
    //computersPriceElement.innerText = computers[0].price + " SEK";
    document.getElementById("computerPrice").innerHTML = currencyValue.format(computersPriceElement);
    computersPicElement.src = "https://noroff-komputer-store-api.herokuapp.com/" + computers[0].image;
    compFeaturesElement.innerHTML = computers[0].description;
    compSpecsElement.innerHTML = computers[0].specs;
    selectedComputerElement.innerHTML = computers[0].title;
}

const addComputerToSite = (computer) => {
    const computerElement = document.createElement("option");
    computerElement.value = computer.id;
    computerElement.appendChild(document.createTextNode(computer.title));
    computersElement.appendChild(computerElement);
}

const handleComputerSelectedChange = e => {
    const selectedComputer = computers[e.target.selectedIndex];
   // computersPriceElement.innerText = selectedComputer.price + " NOK";
   // computerPrices = selectedComputer.price;
   computersPriceElement = currencyValue.format(selectedComputer.price);
document.getElementById("computerPrice").innerHTML = computersPriceElement;
    computersPicElement.src = "https://noroff-komputer-store-api.herokuapp.com/" + selectedComputer.image;
    compFeaturesElement.innerHTML = selectedComputer.description;
    compSpecsElement.innerHTML = selectedComputer.specs;
}

computersElement.addEventListener("change", handleComputerSelectedChange);

const handleComputerPicture = e => {
    const selectedComputerPicture = computers[e.target.selectedIndex];
    computersPicElement.pic = selectedComputerPicture.image;
}

computersPicElement.addEventListener("change", handleComputerPicture);
hideButton();
payBalance()
/*-------------------------Loan---------------------------*/

//Function that let's the user borrow money
loansElement.addEventListener('click', getLoan);

function getLoan() {
if(hasLoan === 0){
    loan = balance * 2;
    loanAmount = prompt('Type the amount you want to borrow');
    loanAmount = parseInt(loanAmount);
    if(loanAmount <= loan){
        hasLoan += loanAmount
    }
    else if(balance > loanAmount){
        hasLoan += loanAmount;
    } 
    else{
        alert("not allowed");
    }
        updateLoan();
        hideButton();
}
else if(hasLoan > 0){
    alert("You are not allowed to take more loans");
}
else{
    alert("Finish your loan first");
}
}



/*-------------------------Work---------------------------*/

workElement.addEventListener('click', getPaid);
//Adds pay to your salary
function getPaid(){
    salary += payPerClick;
    updateSalary();
}


/*------------------------Bank-----------------------------*/

// Function that checks if the user has a loan and when transferring money to bank, will take 10% interest to the bank and the rest will go to the balance.

bankElement.addEventListener('click', payBalance);

function payBalance(){
    let gross_salary = parseInt(salary);
    let tax = gross_salary * 0.1;
    let net_income = gross_salary - tax;
    if(hasLoan != 0){
        balance += net_income;
        hasLoan += tax;
        salary = 0;
    }
    else if(hasLoan === 0){
        balance += salary;
        salary = 0;
    }
    updateBalance();
    updateLoan();
    updateSalary();

}

//----------------payLoan--------------------

// Function that 

repayLoanElement.addEventListener('click', payLoan);

function payLoan(){
if(hasLoan >= 0){
    if(salary>=hasLoan){
        salary -= hasLoan;
        balance  += salary;
        hasLoan=0;
        salary=0;
    }
    else{
        hasLoan -= salary;
       salary = 0;
    }
       if(hasLoan < 0){
        alert(hasLoan);
        balance += hasLoan;
        balance += salary;
        hasLoan = 0;
        salary = 0;
}
   }
   else{
        balance += salary;
        salary = 0;
        hasLoan = 0;
   }
        updateBalance();
        updateLoan();
        updateSalary();
}

//----------------Has loan-------------------
// function that hides the button
function hideButton(){
    if(hasLoan != 0){
        document.getElementById('repayLoan').style.visibility = "visible";
    }
    else{
        document.getElementById("repayLoan").style.visibility = "hidden";
    }  
}

/*----------------Buy now button--------------*/

//Function that checks if a user can buy a laptop or not.
buyElement.addEventListener('click', payComputer);

function payComputer(){
    if(balance >= computersPriceElement){
        balance -= computersPriceElement;
        updateBalance();
        alert("You are now the owner of the selected laptop!")
    }
    else{
        alert("You can't afford this computer!");
    }
}


//Updates html text to match balance
//##########################################
function updateBalance(){
    document.getElementById("totalBalance").innerHTML = currencyValue.format(balance);
}
function updateSalary(){
    document.getElementById("totalPay").innerHTML = currencyValue.format(salary);
}
function updateLoan(){
    document.getElementById("totalLoan").innerHTML = currencyValue.format(hasLoan);
hideButton();
}